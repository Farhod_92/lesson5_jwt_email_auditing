package uzb.farhod.lesson5_jwt_email_auditing.payload;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RegisterDto {

    @NotNull
    @Size(min =3, max = 50)
    private String firstName;

    @NotNull
    @Length(min=3, max=50)
    private String lastName;

    @NotNull
    @Email
    private String email;//username sifatida ketadi

    @NotNull
    private String password;

}
