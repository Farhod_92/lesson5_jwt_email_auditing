package uzb.farhod.lesson5_jwt_email_auditing.entity.enums;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_MODER,
    ROLE_USER;
}
