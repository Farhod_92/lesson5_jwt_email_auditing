package uzb.farhod.lesson5_jwt_email_auditing.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson5_jwt_email_auditing.entity.User;
import uzb.farhod.lesson5_jwt_email_auditing.entity.enums.RoleName;
import uzb.farhod.lesson5_jwt_email_auditing.payload.ApiResponse;
import uzb.farhod.lesson5_jwt_email_auditing.payload.LoginDto;
import uzb.farhod.lesson5_jwt_email_auditing.payload.RegisterDto;
import uzb.farhod.lesson5_jwt_email_auditing.repository.RoleRepository;
import uzb.farhod.lesson5_jwt_email_auditing.repository.UserRepository;
import uzb.farhod.lesson5_jwt_email_auditing.security.JwtProvider;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtProvider jwtProvider;

    public ApiResponse registerUser(RegisterDto registerDto){
        boolean existsByEmail = userRepository.existsByEmail(registerDto.getEmail());
        if(existsByEmail)
            return new ApiResponse("bunday email mavjud", false);

        User user=new User();
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());
        user.setPassword(passwordEncoder.encode(registerDto.getPassword()));
        user.setRoles(Collections.singleton(roleRepository.findByName(RoleName.ROLE_USER)));
        user.setEmailCode(UUID.randomUUID().toString());
        ApiResponse sendEmail = sendEmail(user.getEmail(), user.getEmailCode());

        if(sendEmail.isSuccess()) {
            userRepository.save(user);
            return new ApiResponse("pochtangizga kirib ro'yxatdan o'tishni tsqdiqlang", true);
        }
            return new ApiResponse("xatolik", false);
    }

    public ApiResponse sendEmail(String sendingEmail, String emailCode){
        try{
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom("f.x.1992@inbox.ru");
            mailMessage.setTo(sendingEmail);
            mailMessage.setSubject("Akkauntni tasdiqlash");
            mailMessage.setText("<a href='http:/localhost:8080/api/auth/verifyEmail?emailCode=" + emailCode + "&email=" + sendingEmail + "'>Tasqidlash</a>");

            javaMailSender.send(mailMessage);
            return new ApiResponse("sent",true);
        }catch (Exception e){
            return new ApiResponse(e.getMessage(),false);
        }
    }

    public ApiResponse verifyEmail(String emailCode, String email){
        Optional<User> optionalUser = userRepository.findByEmailCodeAndEmail(emailCode, email);
        if(optionalUser.isPresent()){
            User user= optionalUser.get();
            user.setEnabled(true);
            user.setEmailCode(null);
            userRepository.save(user);
            return new ApiResponse("akkaunt tasdiqlandi", true);
        }

        return new ApiResponse("akkaunt tasdiqlanmadi", false);
    }

    public ApiResponse login(LoginDto loginDto) {
        try{
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    loginDto.getUsername(),
                    loginDto.getPassword()));

            User user = (User) authentication.getPrincipal();
            String token = jwtProvider.generateToken(loginDto.getUsername(), user.getRoles());
            return new ApiResponse("Token", true, token);
        }catch (BadCredentialsException e){
            return new ApiResponse("autentifikatsiyadan o'tmadingiz: -> "+e.getMessage(), false);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByEmail(username);
        //return optionalUser.orElse(null);
        return optionalUser.orElseThrow(()->new UsernameNotFoundException(username + " topilmadi"));
    }
}
