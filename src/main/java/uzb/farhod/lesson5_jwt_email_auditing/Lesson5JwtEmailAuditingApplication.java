package uzb.farhod.lesson5_jwt_email_auditing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson5JwtEmailAuditingApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson5JwtEmailAuditingApplication.class, args);
    }

}
//https://docs.google.com/document/d/1p39pWX1zDLWoRQcmY5InjwYM3p0s687OdPPmmyEW0c8/edit?usp=sharing
