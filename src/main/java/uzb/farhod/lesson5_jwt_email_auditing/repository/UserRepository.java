package uzb.farhod.lesson5_jwt_email_auditing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson5_jwt_email_auditing.entity.User;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    boolean existsByEmail(String email);
    Optional<User> findByEmailCodeAndEmail(String emailCode, String email);
    Optional<User> findByEmail(String email);
}
