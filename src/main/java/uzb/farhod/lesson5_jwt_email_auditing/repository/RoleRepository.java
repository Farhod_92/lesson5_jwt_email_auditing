package uzb.farhod.lesson5_jwt_email_auditing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson5_jwt_email_auditing.entity.Role;
import uzb.farhod.lesson5_jwt_email_auditing.entity.enums.RoleName;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByName(RoleName roleName);
}
