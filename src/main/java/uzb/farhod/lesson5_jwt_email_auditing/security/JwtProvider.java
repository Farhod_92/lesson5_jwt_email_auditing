package uzb.farhod.lesson5_jwt_email_auditing.security;

import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;
import uzb.farhod.lesson5_jwt_email_auditing.entity.Role;
import io.jsonwebtoken.Jwts;

import java.util.Date;
import java.util.Set;

@Component
public class JwtProvider {
    private String secretKey="my key is very strong";
    private long expireTime=1000*60*60*24*2; //2kun

    public String generateToken(String username, Set<Role> roles){
        Date expireDate=new Date(System.currentTimeMillis()+expireTime);
        String token = Jwts.builder()
                .setSubject(username)
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setIssuedAt(new Date())
                .setExpiration(expireDate)
                .claim("roles", roles)
                .compact();
        return token;
    }

    public String getEmailFromToken(String token){
        try{
            String email = Jwts.parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();
            return email;
        }catch (Exception e){
            return null;
        }

    }

}
